package com.hw.db.dao;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class UserDaoTest {

    @Test
    @DisplayName("Change user fullname")
    void ChangeUserTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO forum = new UserDAO(mockJdbc);
        User user = new User();
        user.setNickname(RandomString.make(10));
        user.setFullname("test");
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq(
                        "UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"),
                anyString(), anyString());
    }

    @Test
    @DisplayName("Change user email")
    void ChangeUserTest2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO forum = new UserDAO(mockJdbc);
        User user = new User();
        user.setNickname(RandomString.make(10));
        user.setEmail("test");
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq(
                        "UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"),
                anyString(), anyString());
    }

    @Test
    @DisplayName("Change user about")
    void ChangeUserTest3() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        UserDAO forum = new UserDAO(mockJdbc);
        User user = new User();
        user.setNickname(RandomString.make(10));
        user.setAbout("test");
        UserDAO.Change(user);
        verify(mockJdbc).update(Mockito.eq(
                        "UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"),
                anyString(), anyString());
    }
}
