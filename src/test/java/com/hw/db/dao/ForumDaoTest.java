package com.hw.db.dao;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ForumDaoTest {



    @Test
    @DisplayName("User gets list of users test only with slug")
    void UserListTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ForumDAO.UserList("slug", null, null, null);
        verify(mockJdbc).query(Mockito.eq(
                        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"),
                Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("User gets list of users test  with slug, limit")
    void UserListTest2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ForumDAO.UserList("slug", 2, null, null);
        verify(mockJdbc).query(Mockito.eq(
                        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname LIMIT ?;"),
                Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("User gets list of users test with slug, limit and since")
    void UserListTest3() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ForumDAO.UserList("slug", 2, "01.01.1970", null);
        verify(mockJdbc).query(Mockito.eq(
                        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;"),
                Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("User gets list of users test with slug and since")
    void UserListTest4() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ForumDAO.UserList("slug", null, "01.01.1970", null);
        verify(mockJdbc).query(Mockito.eq(
                        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"),
                Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }


    @Test
    @DisplayName("User gets list of users test with slug desc")
    void UserListTest5() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ForumDAO.UserList("slug", null, null, true);
        verify(mockJdbc).query(Mockito.eq(
                        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc;"),
                Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("User gets list of users test with slug, limit and desc")
    void UserListTest6() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ForumDAO.UserList("slug", 2, null, true);
        verify(mockJdbc).query(Mockito.eq(
                        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc LIMIT ?;"),
                Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("User gets list of users test with slug, limit, since and desc")
    void UserListTest7() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ForumDAO.UserList("slug", 2, "01.01.1970", true);
        verify(mockJdbc).query(Mockito.eq(
                        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
                Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("User gets list of users test with slug, since and desc")
    void UserListTest8() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ForumDAO.UserList("slug", null, "01.01.1970", true);
        verify(mockJdbc).query(Mockito.eq(
                        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;"),
                Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

}
