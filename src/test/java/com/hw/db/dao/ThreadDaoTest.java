package com.hw.db.dao;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;

public class ThreadDaoTest {

    @Test
    @DisplayName("Sort with desc false")
    void testTestDao1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ThreadDAO threadDAO = new ThreadDAO(mockJdbc);
        ThreadDAO.treeSort(100, 100, 20, false);

        Mockito.verify(mockJdbc).query(
                Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.any());
    }

    @Test
    @DisplayName("Sort with desc true")
    void testTestDao2() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ThreadDAO threadDAO = new ThreadDAO(mockJdbc);
        ThreadDAO.treeSort(100, 100, 20, true);

        Mockito.verify(mockJdbc).query(
                Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.any());
    }
}
