package com.hw.db.dao;

import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class PostDaoTest {

    Post oldPost;
    JdbcTemplate jdbcMock = Mockito.mock(JdbcTemplate.class);
    PostDAO postDAO = new PostDAO(jdbcMock);

    @BeforeEach
    void setUp() {
        Post post = new Post();
        post.setAuthor("Daniil");
        post.setMessage("Test");
        post.setCreated(Timestamp.valueOf(LocalDateTime.now()));
        post.setId(10);
        this.oldPost = post;

        Mockito.when(jdbcMock.queryForObject(
                        Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                        Mockito.any(PostDAO.PostMapper.class),
                        Mockito.any()))
                .thenReturn(oldPost);
    }

    @Test
    @DisplayName("Change post author")
    void ChangePostTest1() {
        Post post = new Post();
        post.setAuthor("Daniil2");
        post.setMessage(oldPost.getMessage());
        post.setCreated(oldPost.getCreated());
        post.setId(oldPost.getId());
        PostDAO.setPost(oldPost.getId(), post);

        Mockito.verify(jdbcMock).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"),
                Mockito.anyString(),
                Mockito.anyInt()
        );
    }

    @Test
    @DisplayName("Change post message")
    void ChangePostTest2() {
        Post post = new Post();
        post.setAuthor(oldPost.getAuthor());
        post.setMessage("New Message");
        post.setCreated(oldPost.getCreated());
        post.setId(oldPost.getId());
        PostDAO.setPost(oldPost.getId(), post);

        Mockito.verify(jdbcMock).update(
                Mockito.eq("UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"),
                Mockito.anyString(),
                Mockito.anyInt()
        );
    }

    @Test
    @DisplayName("Change post created")
    void ChangePostTest3() {
        Post post = new Post();
        post.setAuthor(oldPost.getAuthor());
        post.setMessage(oldPost.getMessage());
        post.setCreated(Timestamp.valueOf(LocalDateTime.now().plusHours(10)));
        post.setId(oldPost.getId());
        PostDAO.setPost(oldPost.getId(), post);

        Mockito.verify(jdbcMock).update(
                Mockito.eq("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.any(Timestamp.class),
                Mockito.anyInt()
        );
    }

    @Test
    @DisplayName("Change post author and message")
    void ChangePostTest4() {
        Post post = new Post();
        post.setAuthor("TestTest");
        post.setMessage("TestTest");
        post.setCreated(oldPost.getCreated());
        post.setId(oldPost.getId());
        PostDAO.setPost(oldPost.getId(), post);

        Mockito.verify(jdbcMock).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.anyInt()
        );
    }

    @Test
    @DisplayName("Change post author, message and created")
    void ChangePostTest5() {
        Post post = new Post();
        post.setAuthor("TestTest");
        post.setMessage("TestTest");
        post.setCreated(Timestamp.valueOf(LocalDateTime.now().plusHours(10)));
        post.setId(oldPost.getId());
        PostDAO.setPost(oldPost.getId(), post);

        Mockito.verify(jdbcMock).update(
                Mockito.eq(
                        "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.any(Timestamp.class),
                Mockito.anyInt()
        );
    }

    @Test
    @DisplayName("Change post message and created")
    void ChangePostTest6() {
        Post post = new Post();
        post.setAuthor(oldPost.getAuthor());
        post.setMessage("TestTest");
        post.setCreated(Timestamp.valueOf(LocalDateTime.now().plusHours(10)));
        post.setId(oldPost.getId());
        PostDAO.setPost(oldPost.getId(), post);

        Mockito.verify(jdbcMock).update(
                Mockito.eq("UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.anyString(),
                Mockito.any(Timestamp.class),
                Mockito.anyInt()
        );
    }

    @Test
    @DisplayName("Change post author and created")
    void ChangePostTest7() {
        Post post = new Post();
        post.setAuthor("TestTest");
        post.setMessage(oldPost.getMessage());
        post.setCreated(Timestamp.valueOf(LocalDateTime.now().plusHours(10)));
        post.setId(oldPost.getId());
        PostDAO.setPost(oldPost.getId(), post);

        Mockito.verify(jdbcMock).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.anyString(),
                Mockito.any(Timestamp.class),
                Mockito.anyInt()
        );
    }
}
